Share and Enjoy
===============

``sc.newsportal`` would not have been possible without the contribution of the following people:

- André Nogueira
- Érico Andrei
- Héctor Velarde

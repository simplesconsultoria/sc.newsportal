# -*- coding:utf-8 -*-
from setuptools import find_packages
from setuptools import setup

version = '1.0a1'
description = 'Policy package for news portals.'
long_description = (
    open('README.rst').read() + '\n' +
    open('CONTRIBUTORS.rst').read() + '\n' +
    open('CHANGES.rst').read()
)

setup(
    name='sc.newsportal',
    version=version,
    description=description,
    long_description=long_description,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Plone',
        'Framework :: Plone :: 4.3',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: News/Diary',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    keywords='',
    author='Simples Consultoria',
    author_email='produtos@simplesconsultoria.com.br',
    url='https://github.com/simplesconsultoria/sc.newsportal',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    namespace_packages=['sc'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # 'collective.googlenews',
        # 'collective.mailchimp',
        'collective.cover',
        'collective.disqus',
        'collective.liveblog',
        'collective.newsflash',
        'collective.nitf',
        'collective.polls',
        'collective.sitemap',
        'collective.weather',
        'ftw.globalstatusmessage',
        'plone.app.caching',
        'plone.app.contenttypes <1.2a1',
        'plone.app.upgrade',
        'Products.CMFPlone >=4.3',
        'Products.GenericSetup',
        'Products.PloneFormGen',
        'Products.PloneKeywordManager',
        'sc.blog',
        'sc.contentrules.groupbydate',
        'sc.contentrules.metadata',
        'sc.embedder',
        'sc.periodicals',
        'sc.social.like',
        'setuptools',
        'wildcard.foldercontents',
        'zope.app.component',  # dependency of collective.newsflash
        'zope.i18nmessageid',
        'zope.interface',
    ],
    extras_require={
        'test': [
            'plone.app.robotframework',
            'plone.app.testing [robot]',
            'plone.browserlayer',
            'plone.testing',
            'robotsuite',
        ],
    },
    entry_points="""
    [z3c.autoinclude.plugin]
    target = plone
    """,
)

# -*- coding: utf-8 -*-
from collective.nitf.controlpanel import INITFSettings
from plone.registry.interfaces import IRegistry
from sc.newsportal.tests.base import BaseTestCase
from zope.component import getUtility


class AddonsSettingsTestCase(BaseTestCase):

    def setUp(self):
        super(AddonsSettingsTestCase, self).setUp()
        self.registry = getUtility(IRegistry)
        self.nitf_settings = self.registry.forInterface(INITFSettings)
        self.wt = self.portal['portal_workflow']
        self.pm = self.portal['portal_membership']
        self.maxDiff = None

    def test_comments_enabled_globally(self):
        from plone.app.discussion.interfaces import IDiscussionSettings
        settings = self.registry.forInterface(IDiscussionSettings)
        self.assertTrue(settings.globally_enabled)

    def test_collective_cover_searchable_content_types(self):
        from collective.cover.controlpanel import ICoverSettings
        settings = self.registry.forInterface(ICoverSettings)
        expected = [
            u'Collection',
            u'collective.nitf.content',
            u'sc.embedder',
        ]
        self.assertItemsEqual(settings.searchable_content_types, expected)

    def test_collective_nitf_available_genres(self):
        """Genres used portal wide."""
        available_genres = self.nitf_settings.available_genres
        expected = [
            u'Analysis',
            u'Archive material',
            u'Current',
            u'Exclusive',
            u'From the Scene',
            u'Interview',
            u'Obituary',
            u'Opinion',
            u'Polls and Surveys',
            u'Press Release',
            u'Profile',
            u'Retrospective',
            u'Review',
            u'Special Report',
            u'Summary',
            u'Wrap',
        ]
        self.assertItemsEqual(available_genres, expected)

    def test_collective_nitf_available_sections(self):
        """News sections defined."""
        available_sections = list(self.nitf_settings.available_sections)
        expected = [
        ]
        self.assertItemsEqual(available_sections, expected)

    def test_collective_nitf_default_section(self):
        self.assertEqual(self.nitf_settings.default_section, None)

    def test_sc_social_likes_settings(self):
        likes = self.portal['portal_properties'].sc_social_likes_properties
        enabled_portal_types = list(likes.enabled_portal_types)
        expected = [
            u'collective.nitf.content',
            u'Document',
        ]
        self.assertItemsEqual(expected, enabled_portal_types)

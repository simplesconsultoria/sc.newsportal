# -*- coding: utf-8 -*-
from sc.newsportal.config import PROJECTNAME
from sc.newsportal.tests.base import BaseTestCase
from Products.GenericSetup.upgrade import listUpgradeSteps

PROFILE = PROJECTNAME + ':default'


class UpgradeTestCase(BaseTestCase):

    """Ensure product upgrades work."""

    def test_version(self):
        self.assertEqual(
            self.setup_tool.getLastVersionForProfile(PROFILE)[0], u'1000')

    def _match(self, item, source, dest):
        source, dest = tuple([source]), tuple([dest])
        return item['source'] == source and item['dest'] == dest

    def test_to1010_available(self):
        steps = listUpgradeSteps(self.setup_tool, PROFILE, '1000')
        steps = [s for s in steps if self._match(s[0], '1000', '1010')]
        self.assertEqual(len(steps), 1)

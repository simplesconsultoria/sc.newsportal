# -*- coding: utf-8 -*-
from sc.newsportal.tests.base import BaseTestCase


class InstallTestCase(BaseTestCase):

    """Ensure product is properly installed."""

    def test_installed(self):
        from sc.newsportal.config import PROJECTNAME
        self.assertTrue(self.qi.isProductInstalled(PROJECTNAME))

    def test_browser_layer_installed(self):
        from plone.browserlayer.utils import registered_layers
        from sc.newsportal.interfaces import IAddOnInstalled
        self.assertIn(IAddOnInstalled, registered_layers())

    def test_dependencies_installed(self):
        expected = [
            # 'plone.app.contenttypes',  # FIXME: https://github.com/plone/plone.app.contenttypes/issues/178
            'collective.cover',
            'collective.disqus',
            'collective.liveblog',
            'collective.nitf',
            'plone.app.caching',
            'sc.embedder',
            'sc.social.like',
            'wildcard.foldercontents',
        ]
        for dependency in expected:
            self.assertTrue(
                self.qi.isProductInstalled(dependency),
                dependency + u' not installed'
            )

    def test_available_packages(self):
        installable_profiles = self.qi.listInstallableProfiles()
        expected = [
            # 'collective.newsflash',  # FIXME: waiting for new release
            'collective.polls',
            'collective.sitemap',
            'collective.weather',
            'ftw.globalstatusmessage',
        ]
        for profile in expected:
            self.assertIn(profile, installable_profiles)

    def test_content_rules_installed(self):
        from plone.contentrules.rule.interfaces import IRuleAction
        from zope.component import queryUtility
        expected = [
            'sc.contentrules.actions.Contributors',
            'sc.contentrules.actions.Creators',
            'sc.contentrules.actions.ExcludeFromNav',
            'sc.contentrules.actions.Language',
            'sc.contentrules.actions.Rights',
            'sc.contentrules.actions.Subject',
            'sc.contentrules.actions.groupbydate',
        ]
        for name in expected:
            rule = queryUtility(IRuleAction, name=name)
            self.assertIsNotNone(rule)

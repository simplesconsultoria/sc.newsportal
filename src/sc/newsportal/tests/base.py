# -*- coding: utf-8 -*-
from sc.newsportal.testing import INTEGRATION_TESTING

import unittest


class BaseTestCase(unittest.TestCase):

    """Base test case to be used by other tests."""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.qi = self.portal['portal_quickinstaller']
        self.setup_tool = self.portal['portal_setup']

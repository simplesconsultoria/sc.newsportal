# -*- coding: utf-8 -*-
from plone import api
from sc.newsportal.tests.base import BaseTestCase

import unittest


class SitePropertiesTestCase(BaseTestCase):

    def setUp(self):
        self.portal = self.layer['portal']
        self.properties = self.portal['portal_properties'].site_properties
        self.navtree = self.portal['portal_properties'].navtree_properties
        self.types = self.portal['portal_types']
        self.maxDiff = None

    def test_display_of_publication_date_in_byline_is_enabled(self):
        self.assertTrue(self.properties.displayPublicationDateInByline)

    def test_link_integrity_checks_are_enabled(self):
        self.assertTrue(self.properties.enable_link_integrity_checks)

    def test_livesearch_is_disabled(self):
        self.assertFalse(self.properties.enable_livesearch)

    def test_utf8_is_default_charset(self):
        self.assertEqual(self.properties.default_charset, 'utf-8')
        self.assertEqual(self.portal.email_charset, 'utf-8')

    def test_types_searched(self):
        all_types = set(self.types.listContentTypes())
        types_not_searched = set(self.properties.types_not_searched)
        types_searched = list(all_types - types_not_searched)
        expected = [
            'collective.nitf.content',
            'Document',
            'Liveblog',
            'sc.embedder',
        ]
        self.assertItemsEqual(types_searched, expected)

    def test_meta_types_not_to_list_in_navigation(self):
        meta_types_not_to_list = list(self.navtree.metaTypesNotToList)
        expected = [
            'collective.nitf.content',
            'Discussion Item',
            'Document',
            'Event',
            'File',
            'Image',
            'News Item',
        ]
        for i in expected:
            self.assertIn(i, meta_types_not_to_list)


class NavtreePropertiesTestCase(BaseTestCase):

    def setUp(self):
        self.portal = self.layer['portal']
        self.navtree = self.portal['portal_properties'].navtree_properties
        self.types = self.portal['portal_types']
        self.maxDiff = None

    def test_content_types_displayed_on_navigation(self):
        all_types = set(self.types.listContentTypes())
        metaTypesNotToList = set(self.navtree.metaTypesNotToList)
        content_types_displayed = all_types - metaTypesNotToList
        expected = [
            'Collection',
            'Folder',
            'FormFolder',
            'Link',
        ]
        self.assertItemsEqual(content_types_displayed, expected)


class SyndicationPropertiesTestCase(BaseTestCase):

    def setUp(self):
        self.portal = self.layer['portal']
        self.base_registry = 'Products.CMFPlone.interfaces.syndication.ISiteSyndicationSettings'

    @unittest.skip('not configured yet')
    def test_rss_action(self):
        actions = api.portal.get_tool('portal_actions').document_actions
        rss_action = actions.rss
        self.assertTrue(rss_action.visible)

    @unittest.skip('not configured yet')
    def test_syndication_enabled(self):
        record = 'default_enabled'
        enabled = api.portal.get_registry_record(
            '{}.{}'.format(self.base_registry, record))
        self.assertTrue(enabled)

    @unittest.skip('not configured yet')
    def test_syndication_link(self):
        record = 'show_syndication_link'
        show = api.portal.get_registry_record(
            '{}.{}'.format(self.base_registry, record))
        self.assertTrue(show)

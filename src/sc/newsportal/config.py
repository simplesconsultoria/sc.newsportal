# -*- coding: utf-8 -*-
from Products.CMFPlone.interfaces import INonInstallable
from zope.interface import implements

PROJECTNAME = 'sc.newsportal'


class HiddenProfiles(object):
    implements(INonInstallable)

    def getNonInstallableProfiles(self):
        return [
            u'sc.newsportal:uninstall',
            u'sc.newsportal.upgrades.v1010:default'
        ]
